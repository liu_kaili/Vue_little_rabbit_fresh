// node.js导入路径的实例化方法
const path = require('path')
module.exports = {
  // 自动导入less文件 适合less变量 和 less方法
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'less',
      patterns: [
        // 配置哪些文件需要自动导入
        path.join(__dirname, './src/styles/variables.less'),
        path.join(__dirname, './src/styles/mixins.less')
      ]
    }
  },

  chainWebpack: config => {
    // 把10kb的图片gif 自动转换为base64位 (节约加载 适用于数据加载小图标)
    config.module
      .rule('images')
      .use('url-loader')
      .loader('url-loader')
      .tap(options => Object.assign(options, { limit: 10000 }))
      // 开启IP或域名访问webpack服务器权限
    config.devServer.disableHostCheck(true)
  }
}
