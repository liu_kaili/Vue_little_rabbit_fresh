// 数据展示的懒加载 需要配合图片懒加载
// 导入VueUse组件useIntersectionObserver方法
import { useIntersectionObserver } from '@vueuse/core'
// 导入Vue3的ref方法
import { ref } from 'vue'

// useIntersectionObserver 有三个参数
// 参数1 表示被监听的Dom元素(观察容器)
// 参数2 监听是否到达绑定的Dom组件(布尔值类型)
// 参数3 是一个对象用来配置这个方法(通常用来配置触发条件)

export default (apiFn) => { // 参数apiFn表示调用接口的方法(组件传来的api接口 用来获取展示数据)
  // 接收传来的懒加载数据(展示数据)
  const result = ref([])
  // 设置VueUse懒加载的监听的Dom元素(观察容器)
  const target = ref(null)
  // 启动通过VueUse的懒加载操作
  const { stop } = useIntersectionObserver(target, ([{ isIntersecting }]) => {
  // stop是取消监听 target表示被监听的Dom元素(观察容器) isIntersecting是监听是否到达绑定的Dom组件(布尔值类型)
    if (isIntersecting) { // 如果到达了Dom组件可视区 调用接口 获取展示数据
    // 触发一次之后，取消继续监听
      stop()
      // 被监听的Dom组件已经进入可视区，此时组件传来的api接口
      apiFn().then(data => {
        // 储存到展示数据中
        result.value = data.result
      })
    }
  }, {
    // 刚一进入可视区，就触发（默认值表示，进入一段距离之后才触发）
    threshold: 0 // 0-1范围 类似百分比(推荐设置为0 无延迟)
  })
  // 进行返回值
  // 1、target表示被监听的Dom元素(需要的观察容器)
  // 2、result表示调用接口api返回的数据 (展示数据)
  return { target, result }
}
