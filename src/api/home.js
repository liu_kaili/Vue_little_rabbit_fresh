// 导入配置好的 初始化路由组件
import request from '../utils/request'

// 获取顶部导航栏列表数据
export const findHeadCategory = () => {
  return request({
    method: 'get',
    url: '/home/category/head'
  })
}

// 获取品牌数据
export const findBrand = (limit) => {
  return request({
    method: 'get',
    url: '/home/brand',
    data: { limit }
  })
}

// 获取首页轮播图列表数据
export const findBanner = () => {
  return request({
    method: 'get',
    url: '/home/banner'
  })
}

// 获取新鲜好物接口数据
export const findNew = () => {
  return request({
    method: 'get',
    url: 'home/new'
  })
}

// 人气推荐
export const findHot = () => {
  return request({
    method: 'get',
    url: 'home/hot'
  })
}

// 获取商品信息
export const findGoods = () => {
  return request({
    method: 'get',
    url: 'home/goods'
  })
}

// 获取最新专题
export const findSpecial = () => {
  return request({
    method: 'get',
    url: 'home/special'
  })
}

// 根据一级分类的id获取一级分类的详细数据（包括相关的商品数据）
export const findTopCategory = (id) => {
  return request({
    method: 'get',
    url: '/category',
    data: { id }
  })
}

// 获取二级分类的筛选条件数据
export const findSubCategoryFilter = (id) => {
  return request({
    method: 'get',
    url: '/category/sub/filter',
    data: { id }
  })
}

// 二级分类下商品的筛选接口(分页数据)
export const findSubCategoryGoods = (data) => {
  return request({
    method: 'post',
    url: '/category/goods/temporary',
    data
  })
}
