// 导入加载失败默认图片
import defaultImg from '@/assets/images/200.png'
// // 封装UI组件库的组件(导入骨架屏自定义插件)
// import XtxSkeleton from './xtx-skeleton.vue'
// // 导入轮播图组件
// import XtxCarousel from './xtx-carousel.vue'
// // 导入查看全部组件(展示更多)
// import XtxMore from './xtx-more.vue'
// // 导入封装好的面包屑组件
// import XtxBread from './xtx-bread.vue'
// import XtxBreadItem from './xtx-bread-item.vue'
// 导入弹窗全局插件
// import Message from './Message.js'

// -------------------------------------- 批量导入Vue插件
// 参数一：从哪个目录中读取文件
// 参数二：是否读取子目录中的文件：true读取子目录，false不读取子目录
// 参数三：读取文件的匹配规则
const importFn = require.context('./', false, /\.vue$/)
// importFn.keys()返回值是数组，其中放的是组件的文件名称
// console.dir(importFn.keys())

// --------------------------------------- Vue自定义指令
// 自定义图片懒加载指令 (单独图片懒加载适合多图模式 少图模式批量设置即可)
// <img v-lazy="http://abc.com/a.jpg"/>
// 设置Vue3的实例化方法 (Vue模板调用 v-实例化名称)
const defineDirective = (app) => {
  // vue.directive('lazy', {})
  app.directive('lazy', {
    // vue2中用 inserted
    //! vue3中用 mounted
    mounted (el, bindings) {
      // el表示绑定指令的dom
      // bindings表示指令的相关信息
      // 实现图片懒加载的监听
      const observer = new IntersectionObserver(([{ isIntersecting }]) => {
        if (isIntersecting) {
          // 进入可视区之后，取消监听
          observer.unobserve(el)
          // 进入可视区：加载图片的地址
          el.src = bindings.value
          // 监听图片加载失败的情况
          el.onerror = () => {
            // 如果加载图片失败了，就显示默认图片
            el.src = defaultImg
          }
        }
      }, {
        // 刚一进入可视区，就触发（默认值表示，进入一段距离之后才触发）
        threshold: 0 // 0-1范围 类似百分比(推荐设置为0 无延迟)
      })
      // 实现图片DOM的监听
      observer.observe(el)
    }
  })
}

// 自定义一个插件方法导出
export default {
  install (app) {
    // app表示Vue全局组件的实例对象 (Vue2是Vue. Vue3是app.)
    // 配置全局插件
    // app.component(XtxSkeleton.name, XtxSkeleton) // (导入组件的名称的name名.name,导入组件的名称)
    // app.component(XtxCarousel.name, XtxCarousel)
    // app.component(XtxMore.name, XtxMore)
    // app.component(XtxBread.name, XtxBread)
    // app.component(XtxBreadItem.name, XtxBreadItem)
    // 批量注册 -------------------------------------------------------------------
    // 自动化批量注册全局组件
    importFn.keys().forEach(componentPath => {
      // componentPath表示其中一个组件的路径
      // 根据路径导入组件
      // 返回值component表示组件的实例对象
      const component = importFn(componentPath).default
      // 注册全局组局
      app.component(component.name, component)
    })
    defineDirective(app)

    // 把弹窗插件 添加到Vue3的原型对象中
    // Vue2中直接向原型对象中添加属性即可
    // Vue.prototype.$message = Message
    // app.prototype.$message = Message
    // Vue3扩展原型属性，需要按照如下方式实现
    // app.config.globalProperties.$message = Message
  }
}
