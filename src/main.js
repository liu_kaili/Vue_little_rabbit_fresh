// 导入实例化的Vue3
import { createApp } from 'vue'
import App from './App.vue'
// 导入路由组件
import router from './router'
// 导入Vuex组件
import store from './store'
// 导入css样式重置
import 'normalize.css'
// 导入css样式重置的css文件
import './styles/common.less'
// 导入自定义插件 让其实例化可在组件内使用
import XtxUI from './components/library/index'
// 导入Vue3要添加的原型链方法
import Message from './components/library/Message.js'
import Confirm from './components/library/Confirm'
// 创建Vue3要添加的原型链方法(需要先创建虚拟Dom方法)
const ret = {
  install (app) {
    // 通过config.globalProperties给其添加原型链方法
    // app.config.globalProperties.$要创建的原型链方法名 = 创建的虚拟Dom方法
    app.config.globalProperties.$message = Message
    app.config.globalProperties.$confirm = Confirm
  }
}

// 创建一个vue应用实例(.use() 可以实例化导入的插件)
createApp(App).use(ret).use(store).use(router).use(XtxUI).mount('#app')
