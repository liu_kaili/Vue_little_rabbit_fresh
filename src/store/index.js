// 导入Vuex
import { createStore } from 'vuex'
// 导入全局Vuex组件
import global from './modules/global'
// 导入局部的Vuex组件
import user from './modules/user'
import cart from './modules/cart'
import cate from './modules/cate'
// 导入持久化的Vuex插件
import createPersistedstate from 'vuex-persistedstate'
// ------------------------------------
// 创建vuex仓库并导出
export default createStore({
  // 全局的Vuex模块导入 直接 ...导入即可(展开语法)
  ...global,
  modules: {
    // 局部的Vuex模块导入
    user,
    cart,
    cate
  },
  // 配置导入的Vuex插件
  plugins: [
    // 默认储存在local Storage缓存中(生命周期是永久的)
    createPersistedstate({
      // key是储存在缓存中的数据名称(储存数据的键名)
      key: 'erabbit-client-pc-store-128',
      // paths是存储state中需要持久化的数据(Vuex模块的名称)
      // 如果是模块下具体的数据需要加上模块名称，如user.token(储存指定数据)
      paths: ['user', 'cart', 'cate']
    })
  ]
})
