// 全局模块
export default {
  state: {
    // 数据
    info: '测试数据'
  },
  mutations: {
    // 改数据函数
    updateInfo (state, payload) {
      state.info = payload
    }
  },
  actions: {
    // 请求数据函数
    updateInfo (context, payload) {
      setTimeout(() => {
        context.commit('updateInfo', payload)
      }, 1000)
    }
  },
  getters: {
    // vuex的计算属性
    fullInfo (state) {
      return state.info + ' tom'
    }
  }

}
