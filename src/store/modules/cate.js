// 分类信息模块
import { findHeadCategory } from '@/api/home.js'
import { category } from '@/api/constants.js'
export default {
  namespaced: true,
  state: () => {
    return {
      title: '分类模块',
      // 分类列表数据(提供默认数据，防止默认出现长时间的空白效果)
      // [{id: 0, name: '居家'}, ... ]
      list: category.map((item, index) => ({ id: index, name: item }))
    }
  },
  mutations: {
    setList (state, payload) {
      state.list = payload
    },
    // 控制二级分类的显示
    show (state, id) {
      // id表示一级分类的id
      const cate = state.list.find(item => {
        return item.id === id
      })
      cate.open = true
    },
    // 控制二级分类的隐藏
    hide (state, id) {
      // id表示一级分类的id
      const cate = state.list.find(item => {
        return item.id === id
      })
      cate.open = false
    }
  },
  actions: {
    async setList (context) {
      const ret = await findHeadCategory()
      // 给每一个一级分类的数据添加一个open标志位，用于控制他的二级分类的打开和隐藏
      ret.result.forEach(item => {
        item.open = false
      })
      context.commit('setList', ret.result)
    }
  },
  getters: {}
}
